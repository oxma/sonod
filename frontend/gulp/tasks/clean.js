import del          from 'del';
import { path }     from '../path';
import env          from 'gulp-environment';

// Удаление всех файлов, для чистовой сборки перед деплоем
export const Clean = (done) => {
    del.sync(env.if.development(path.clean.dev).else(path.clean.prod))
    done();
};
