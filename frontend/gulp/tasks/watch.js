import { watch } from 'gulp';
import { path } from '../path';
import { Pug } from './pug';
import { Style } from './style';
import { Script } from './script';
import { Font } from './font';
import { Image } from './image';

// Слежение за изменяемыми файлами
export const WatchFiles = (done) => {
    // watch('Путь', tasks)
    watch(path.pug.watch, Pug);
    watch(path.style.watch, Style);
    watch(path.script.watch, Script);
    watch(path.font.watch, Font);
    watch(path.image.watch, Image);
    done();
}