import { src, dest } from 'gulp';
import a11y from 'gulp-accessibility';
import rename from 'gulp-rename';

// Accessibility валидация
export const A11y = (done) => {
    src('./frontend/static/**/*.html')
        .pipe(a11y({
            force: true,
            verbose: true,
            // Некоторые ошибки можно игнорировать в этом массиве
            // ignore: [
            //     ''
            // ],
            reportLevels: {
                notice: false,
                warning: true,
                error: true
            }
        }))
        .pipe(a11y.report({reportType: 'json'}))
        .pipe(rename({
            extname: '.json'
        }))
        .pipe(dest('./test/accessibility'))
        done();
};