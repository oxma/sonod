import { src, dest }        from 'gulp';
import { path }             from '../path';
import env                  from 'gulp-environment';
import autoprefixer         from 'gulp-autoprefixer';
import browserSync          from 'browser-sync';
import concat               from 'gulp-concat';
import cssnano              from 'gulp-cssnano';
import postcss              from 'gulp-postcss';
import postcssReporter      from 'postcss-reporter';
import postcssScss          from 'postcss-scss';
import scss                 from 'gulp-sass';
import sourcemaps           from 'gulp-sourcemaps';
import stylelint            from 'stylelint';

export const Style = (done) => {
    src(path.style.src)
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(scss())
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(concat('style.min.css'))
        .pipe(cssnano())
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(env.if.development(dest(path.style.dist)).else(dest(path.style.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}

// Линтер Scss (.stylelintrc)
export const StyleLinter = (done) => {
    src(path.style.watch)
        .pipe(postcss(
            [
                stylelint(),
                postcssReporter({
                    clearReportedMessages: true,
                    throwError: false,
                }),
            ], {
                syntax: postcssScss
            })
        )
        .pipe(browserSync.stream())
        done();
}

// Сборка подключаемых стилей
export const StyleVendor = (done) => {
    src(path.style.srcVendor)
        .pipe(scss())
        .pipe(concat('vendor.min.css'))
        .pipe(cssnano())
        .pipe(env.if.development(dest(path.style.dist)).else(dest(path.style.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}
