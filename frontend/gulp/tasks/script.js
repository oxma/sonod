import { src, dest }        from 'gulp';
import { path }             from '../path';
import env                  from 'gulp-environment';
import browserSync          from 'browser-sync';
import concat               from 'gulp-concat';
import sourcemaps           from 'gulp-sourcemaps';
import fileInclude          from 'gulp-file-include';
import eslint               from 'gulp-eslint';

// Создание основного скрипта
export const Script = (done) => {
    src(path.script.src)
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(concat('script.min.js'))
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(env.if.development(dest(path.script.dist)).else(dest(path.script.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}

// Создание скрипта с подключаемыми библиотеками
export const ScriptVendor = (done) => {
    src(path.script.srcVendor)
        .pipe(env.if.development(sourcemaps.init()))
        .pipe(fileInclude({
            prefix: '// @',
        }))
        .pipe(concat('vendor.min.js'))
        .pipe(env.if.development(sourcemaps.write('../maps')))
        .pipe(env.if.development(dest(path.script.dist)).else(dest(path.script.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}

// Линтер для основного скрипта
export const ScriptLinter = (done) => {
    src(path.script.watch)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError())
        .pipe(browserSync.stream())
        done();
}