import { src, dest }        from 'gulp';
import { path }             from '../path';
import env                  from 'gulp-environment';
import browserSync          from 'browser-sync';
import iconfont             from 'gulp-iconfont';
import iconfontCss          from 'gulp-iconfont-css';
import replace              from 'gulp-replace';

// Переменные для иконок
const fontName = 'icons';
const fontClass = 'icon';

// Перебрасываем шрифты в статику
export const Font = (done) => {
    src(path.font.src)
        .pipe(env.if.development(dest(path.font.dist)).else(dest(path.font.prod)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
};

// Создание иконочного шрифта из SVG
export const Icon = (done) => {
    src(path.icon.src)
        .pipe(env.if.development(
            iconfontCss({
                cssClass: fontClass,
                fontName: fontName,
                targetPath: path.icon.targetPath,
                fontPath: path.icon.fontPath,
            })
        ).else(
            iconfontCss({
                cssClass: fontClass,
                fontName: fontName,
                targetPath: path.icon.prod.targetPath,
                fontPath: path.icon.prod.fontPath,
            })
        ))
        .pipe(replace('	', '    '))
        .pipe(iconfont({
          fontName: fontName,
          formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
          normalize: true,
          autohint: false,
          fontHeight: 1001
        }))
        .pipe(env.if.development(dest(path.icon.dist)).else(dest(path.icon.prod.dist)))
        .pipe(env.if.development(browserSync.reload({stream: true})))
        done();
}