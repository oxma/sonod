// Констатна с путями сборки
export const path = {
    build: './frontend/static/',

    clean: {
        dev: [
            './frontend/static/',
            './test/accessibility/',
            './frontend/src/assets/scss/_icon.scss',
            './frontend/src/assets/fonts/icon/'
        ],
        prod: [
            './backend/assets/', 
            './frontend/src/assets/scss/_icon.scss'
        ] 
    },

    pug:  {
        src: [
            './frontend/src/pages/*.pug',
            './frontend/src/pages/**/*.pug'
        ],
        dist: './frontend/static/',
        watch: [
            './frontend/src/pages/*.pug',
            './frontend/src/pages/**/*.pug',
            './frontend/src/components/*.pug',
            './frontend/src/components/**/*.pug',
            './frontend/src/sections/*.pug',
            './frontend/src/sections/**/*.pug',
            './frontend/src/layouts/*.pug',
            './frontend/src/layouts/**/*.pug',
            './frontend/configuration/*.pug'
        ],
        validate: [
            './frontend/static/*.html',
            './frontend/static/**/*.html'
        ]
    },

    style: {
        src: './frontend/src/assets/scss/style.scss',
        srcVendor: './frontend/src/assets/scss/vendor.scss',
        dist: './frontend/static/assets/css/',
        watch: [
            './frontend/src/assets/scss/*.scss',
            './frontend/src/components/*.scss',
            './frontend/src/components/**/*.scss',
            './frontend/src/sections/*.scss',
            './frontend/src/sections/**/*.scss',
            './frontend/src/layouts/*.scss',
            './frontend/src/layouts/**/*.scss'
        ],
        prod: './backend/assets/css/'
    },

    script: {
        src: './frontend/src/assets/js/main.js',
        srcVendor: './frontend/src/assets/js/vendor.js',
        dist: './frontend/static/assets/js/',
        watch: [
            './*.js',
            './frontend/gulp/**/*.js',
            './frontend/src/assets/js/*.js',
            './frontend/src/sections/**/*.js',
            './frontend/src/components/**/*.js'
        ],
        prod: './backend/assets/js/'
    },

    font: {
        src: './frontend/src/assets/fonts/**/*.*',
        dist: './frontend/static/assets/fonts/',
        watch: './frontend/src/assets/fonts/**/*.*',
        prod: './backend/assets/fonts/'
    },

    image: {
        src: [
            './frontend/src/assets/img/**/*.*',
            '!./frontend/src/assets/img/icons/*.*'
        ],
        dist: './frontend/static/assets/img/',
        watch: './frontend/src/assets/img/**/*.*',
        prod: './backend/assets/img/'
    },

    icon: {
        src: './frontend/src/assets/img/icons/*.svg',
        dist: './frontend/src/assets/fonts/icons/',
        targetPath: '../../scss/_icon.scss',
        fontPath: '../fonts/icons/',
        watch: './frontend/src/assets/img/icons/*.svg',
        prod: {
            dist: './backend/assets/fonts/icons/',
            fontPath: '../fonts/icons/',
            targetPath: '../../../../frontend/src/assets/scss/_icon.scss',
        }
    }
};