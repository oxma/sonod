/* eslint-disable no-unused-vars */

// Глобавльные переменные для проекта
let vars = {};

vars.$window = $(window);
vars.$document = $(document);
vars.$html = $(document.documentElement);
vars.$body = $(document.body);

// Preloader
vars.$preloader = $('.preloader');