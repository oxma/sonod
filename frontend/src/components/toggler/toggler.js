const toggler = (idElement, beforeText, afterText, idBlockToggler) => {
    $(idElement).on('click', function(){
        $(this).html($(this).html() == afterText ? beforeText : afterText);
        $(idBlockToggler).toggle();
    });
};

toggler('#toggler', 'Скрыть', 'Показать', '#descr');