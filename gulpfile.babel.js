import { parallel, series} from 'gulp';
import { Serve } from './frontend/gulp/tasks/server';
import { Pug, PugLinter, W3C } from './frontend/gulp/tasks/pug';
import { WatchFiles } from './frontend/gulp/tasks/watch';
import { Style, StyleVendor, StyleLinter } from './frontend/gulp/tasks/style';
import { Script, ScriptVendor, ScriptLinter } from './frontend/gulp/tasks/script';
import { Font, Icon } from './frontend/gulp/tasks/font';
import { Image } from './frontend/gulp/tasks/image';
import { Clean } from './frontend/gulp/tasks/clean';
import { A11y } from './frontend/gulp/tasks/a11y';

// Запуск сервера + watch
let RunServer = parallel(
    WatchFiles,
    Serve
);

// Запуск линтеров
let RunLinter = series(
    PugLinter,
    StyleLinter,
    ScriptLinter
);

// Dev-сборка, с запуском сервера
let Build = series(
    Image,
    Font,
    StyleVendor,
    Style,
    ScriptVendor,
    Script,
    Pug,
    RunServer
);

// Сборка для тестов и code-style
let Test = parallel(
    RunLinter,
    W3C,
    A11y
);

// Deploy сборка, с минификацией
let Deploy = series(
    Pug
);

// Таски
exports.clean       = Clean;
exports.icon        = Icon;
exports.default     = Build;
exports.linter      = RunLinter;
exports.deploy      = Deploy;
exports.test        = Test;